import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';

import { AuthService} from './_services/auth.service';
import { LoginService} from './_services/login.service';
import { ConfigService} from './_services/config.service';
import { MessageService} from './_services/message.service';
import { ICCService} from './_services/icc.service';
import { TeamService} from './_services/team.service';
import { PersonalService} from './_services/personal.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [
    LoginService,
    AuthService,
    ConfigService,
    MessageService,
    ICCService,
    TeamService,
    PersonalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
