import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router} from '@angular/router';
import { Location } from '@angular/common';
import { RouterTestingModule} from '@angular/router/testing';
import { AppRoutingModule, routes } from '../app-routing.module';
import { FormsModule } from '@angular/forms';
import { trigger, style, transition, animate, keyframes, query, stagger} from '@angular/animations';
import { AuthService} from '../_services/auth.service';
import { MessageService} from '../_services/message.service';
import { ICCService} from '../_services/icc.service';
import { ICC } from '../_models/icc';
import { TeamService} from '../_services/team.service';
import { Team } from '../_models/team';
import { PersonalService} from '../_services/personal.service';
import { Personal } from '../_models/personal';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ConfigService } from '../_services/config.service';
import { Config } from '../_models/config';
import { HomeComponent } from './home.component';
import { LoginComponent } from '../login/login.component';
import { LogoutComponent } from '../logout/logout.component';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

describe('HomeComponent with Spies', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let router: Router;
  let location: Location;
  let messageService: MessageService;
  let configService: ConfigService;
  let iccService: ICCService;
  let teamService: TeamService;
  let personalService: PersonalService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent,
                      LoginComponent,
                      LogoutComponent
                    ],
      imports: [ RouterTestingModule.withRoutes(routes),
                FormsModule
              ],
      providers: [
              AuthService,
              [{provide: TeamService, useValue: teamService}],
              [{provide: PersonalService, useValue: personalService}],
              [{provide: ICCService, useValue: iccService}],
              [{provide: MessageService, useValue: messageService}],
              [{provide: ConfigService, useValue: configService}],
              HttpClient,
              HttpHandler
      ],
    })
    .compileComponents();

    configService = jasmine.createSpyObj('ConfigService',
    {'getApiUrl': '',
     'constructor': '',
     'getConfig': Observable.of('')
    });

    messageService = jasmine.createSpyObj('MessageService', {
      'getMessages': Observable.of(['Message1', 'Message2', 'Message3']),
      'returnPromise': new Promise((resolve, reject) => {
        resolve('mjh: Mock promise result');
      })
    });

    iccService = jasmine.createSpyObj('ICCService', {
      'getIccs': Observable.of([ {'id': 1, 'name': 'Brian', 'desc': 'icc1', 'status': 'todo'},
      {'id': 2, 'name': 'Brian', 'desc': 'icc2', 'status': 'todo'}])
    });

    teamService = jasmine.createSpyObj('TeaamService', {
      'getTeams': Observable.of([ {'id': 1, 'name': 'Brian', 'desc': 'team1', 'status': 'todo'},
      {'id': 2, 'name': 'Brian', 'desc': 'team2', 'status': 'todo'}])
    });

    personalService = jasmine.createSpyObj('PersonalService', {
      'getPersonals': Observable.of([ {'id': 1, 'name': 'Brian', 'desc': 'personal1', 'status': 'todo'},
      {'id': 2, 'name': 'Brian', 'desc': 'personal2', 'status': 'todo'}])
    });

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    router.initialNavigation();
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('add a message', () => {
    component.ngOnInit();
    expect(component.messages[0]).toBe('Message1');
    expect(component.messages[1]).toBe('Message2');
    expect(component.messages[2]).toBe('Message3');
    component.newMessage = 'Message4';
    component.addMessage();
    expect(component.messages[3]).toBe('Message4');
  });

  it('get Done and Back', () => {
    component.ngOnInit();
    expect(component.iccs[0].desc).toBe('icc1');
    expect(component.personals[0].desc).toBe('personal1');
    expect(component.teams[0].desc).toBe('team1');
    expect(component.iccsToDo[0]).toBe('icc1');
    expect(component.personalsToDo[0]).toBe('personal1');
    expect(component.teamsToDo[0]).toBe('team1');
    component.iccDone(0);
    component.personalDone(0);
    component.teamDone(0);
    expect(component.iccsDone[0]).toBe('icc1');
    expect(component.personalsDone[0]).toBe('personal1');
    expect(component.teamsDone[0]).toBe('team1');
    expect(component.iccsToDo[0]).toBe('icc2');
    expect(component.personalsToDo[0]).toBe('personal2');
    expect(component.teamsToDo[0]).toBe('team2');
    component.iccDone(0);
    component.personalDone(0);
    component.teamDone(0);
    component.iccBack(0);
    component.personalBack(0);
    component.teamBack(0);
    expect(component.iccsDone[0]).toBe('icc2');
    expect(component.personalsDone[0]).toBe('personal2');
    expect(component.teamsDone[0]).toBe('team2');
    expect(component.iccsToDo[0]).toBe('icc1');
    expect(component.personalsToDo[0]).toBe('personal1');
    expect(component.teamsToDo[0]).toBe('team1');
  });

});

