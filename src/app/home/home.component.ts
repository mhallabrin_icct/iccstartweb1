import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, style, transition, animate, keyframes, query, stagger} from '@angular/animations';
import { AuthService} from '../_services/auth.service';
import { MessageService} from '../_services/message.service';
import { ICCService} from '../_services/icc.service';
import { ICC } from '../_models/icc';
import { TeamService} from '../_services/team.service';
import { Team } from '../_models/team';
import { PersonalService} from '../_services/personal.service';
import { Personal } from '../_models/personal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('change', [
      transition('* => *', [

        query(':enter', style({ opacity: 0 }), {optional: true}),

        query(':enter', stagger('300ms', [
          animate('.6s ease-in', keyframes([
            style({opacity: 0, transform: 'translateY(-75%)', offset: 0}),
            style({opacity: .5, transform: 'translateY(35px)',  offset: 0.3}),
            style({opacity: 1, transform: 'translateY(0)',     offset: 1.0}),
          ]))]), {optional: true})
      ])
    ])


  ]
})
export class HomeComponent implements OnInit {

  userAuth: boolean;
  userName: string;
  userRole: string;
  messages: string[];
  newMessage: string;
  iccs: ICC[];
  iccsToDo = [];
  iccsDone = [];
  teams: Team[];
  teamsToDo = [];
  teamsDone = [];
  personals: Personal[];
  personalsToDo = [];
  personalsDone = [];

  constructor(
    private _auth: AuthService,
    private _messageService: MessageService,
    private _iccService: ICCService,
    private _teamService: TeamService,
    private _personalService: PersonalService,
    private router: Router
  ) { }

  ngOnInit() {
    this._auth.name.subscribe(res => this.userName = res);
    this._auth.role.subscribe(res => this.userRole = res);
    this._auth.authenticated.subscribe(res => this.userAuth = res);
    if (this.userAuth === false) {
      this.router.navigate(['']);
    }

    this._messageService.getMessages()
      .subscribe(data => {this.messages = data;
      }, (error) => {
        console.log('mjh1 ' + error);
      });

    this._iccService.getIccs(this.userName)
    .subscribe(data => {
      this.iccs = data;
      for (const icc of this.iccs) {
        if (icc.status === 'done') {
          this.iccsDone.push(icc.desc);
        } else {
          this.iccsToDo.push(icc.desc);
        }
      }
    }, (error) => {
      console.log('mjh2 ' + error);
    });

    this._teamService.getTeams(this.userName)
    .subscribe(data => {this.teams = data;
      for (const team of this.teams) {
        if (team.status === 'done') {
          this.teamsDone.push(team.desc);
        } else {
          this.teamsToDo.push(team.desc);
        }
      }
    }, (error) => {
      console.log('mjh3 ' + error);
    });

    this._personalService.getPersonals(this.userName)
    .subscribe(data => {
      this.personals = data;
      for (const personal of this.personals) {
        if (personal.status === 'done') {
          this.personalsDone.push(personal.desc);
        } else {
          this.personalsToDo.push(personal.desc);
        }
      }
    }, (error) => {
      console.log('mjh4 ' + error);
    });
  }

  addMessage() {
    this.messages.push(this.newMessage);
    this.newMessage = '';
    console.log('mjh: calling Promise');
    this.callPromise();
  }

  iccDone(i) {
    this.iccsDone.push(this.iccsToDo[i]);
    this.iccsToDo.splice(i, 1);
  }

  iccBack(i) {
    this.iccsToDo.push(this.iccsDone[i]);
    this.iccsDone.splice(i, 1);
  }

  teamDone(i) {
    this.teamsDone.push(this.teamsToDo[i]);
    this.teamsToDo.splice(i, 1);
  }

  teamBack(i) {
    this.teamsToDo.push(this.teamsDone[i]);
    this.teamsDone.splice(i, 1);
  }

  personalDone(i) {
    this.personalsDone.push(this.personalsToDo[i]);
    this.personalsToDo.splice(i, 1);
  }

  personalBack(i) {
    this.personalsToDo.push(this.personalsDone[i]);
    this.personalsDone.splice(i, 1);
  }

  callPromise() {
    this._messageService.returnPromise().then((r => console.log('mjh: ' + r)));
  }
}
