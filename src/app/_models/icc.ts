export interface ICC {
    id: number;
    name: string;
    desc: string;
    status: string;
  }
