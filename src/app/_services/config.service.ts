import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../_models/config';

@Injectable()
export class ConfigService {
  private config: Config;
  public configUrl = 'assets/_data/config';
  // public configUrl = '/Users/mhallabrin/code/familyWeb/src/assets/_data/config';

  constructor(private http: HttpClient) {}

  getConfig() {
    this.http.get<Config>(this.configUrl)
    .subscribe(data => {this.config = { ...data };
      },
        error => {console.log('mjh9: caught an error = ' + error);
      }
      );
  }

  getLoginUrl() {
    return this.config.loginUrl;
  }

  getApiUrl() {
    console.log('2configUrl = ' + this.configUrl);
    return this.config.apiUrl;
  }

  getPost() {
    return this.config.post;
  }
}
