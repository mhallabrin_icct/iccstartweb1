import { Injectable } from '@angular/core';
import { AuthService} from './auth.service';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from './config.service';

//  may use someday
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';

import { LoginResponse} from '../_models/loginResponse';

@Injectable()
export class LoginService {

  constructor(private authService: AuthService,
              private configService: ConfigService,
              private http: HttpClient) {}

  checkCredentials(userID: string, pw: string) {

      if (this.configService.getPost() === 'get') {
        const promise = new Promise((resolve, reject) => {
          this.http.get<LoginResponse>(this.configService.getLoginUrl() + 'login/' + userID)
          .subscribe(res => {
              this.authService.setName(res.name);
              this.authService.setRole(res.role);
              this.authService.setAuth(true);
              resolve(res.message);
            },
            (err: HttpErrorResponse) => {
              if (err.error instanceof Error) {
                reject('Client-side error occured.');
              } else {
                console.log('Server-side error occured.');
                resolve('Credentials Not found, please try again');
              }
            }
          );
        });
        return promise;

      } else {
        const promise = new Promise((resolve, reject) => {
          this.http.post<LoginResponse>(this.configService.getLoginUrl() + 'login', { userid: userID, password: pw })
          .subscribe(res => {
              this.authService.setName(res.name);
              this.authService.setRole(res.role);
              this.authService.setAuth(true);
              resolve(res.message);
            },
            (err: HttpErrorResponse) => {
              if (err.error instanceof Error) {
                reject('Client-side error occured.');
              } else {
                console.log('Server-side error occured.');
                resolve('Credentials Not found, please try again');
              }
            }
          );
        });
        return promise;
      }
    }
}
