import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICC } from '../_models/icc';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ICCService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {}

  getIccs(name: string): Observable<ICC[]> {
    return this.http.get<ICC[]>(this.configService.getApiUrl() + 'icc/' + name);
  }

  updateIcc(id: number, status: string) {
    return 'ICC updated';
  }
}
