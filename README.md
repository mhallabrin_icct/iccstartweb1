# WebStarter - ICC Automation Team 

##  Who should read this document
This readme is primarily intended for ICC automation resources or SDET's to learn about the ICC automation framework for Web/RWD applications.  This readme should also be a reference doc and the source of truth for Web/RWD automation.  This document uses generally accepted technical jargon and basic web, user interface, mobile, software development and automation terminologies. This document is not intended for non-developers (I.E. Hybrid Testers), however any ICC resource can reICC an SDET to install on their local machine to see the WebStarter application and automation in action.


## Purpose of WebStarter project 
The purpose of the ICC Diagnostic's WebStarter project is to ramp up on web application automation testing. This standalone project contains all the assets to start a new ICC automation project and to ramp up new automation team resources.

The WebStarter's frameworks and tools are all Open Source projects and considered best in class.  As such ICC Automation team gets the benefit of the Open Source communities and thousands of developers to enhance, fix and support these projects with no investment.  There needs to be limited "framework" development or development on top of these projects, as this will allow for easy upgrades.

This project is dynamic and will change often with:
  0. New versions of the open source projects / packages (keeping up to date with the modern browsers and development tools)
  1. Additional or replacement frameworks
  2. Updates to the application to demonstrate proper automation coding principles
  3. Updated automation sample code
  4. Enhanced global reusable functions


## Purpose of this readme.md 
This readme.me is the set of steps to use all features of the WebStarter project.  These steps are exhaustive from an imperative (not declarative perspective).  They indicate what needs to happen (imperative), and not all the granular steps (declarative) that are requirement.  These granular steps are readily available on each project websites, YouTube video and readily available tutorials on the internet. To go into detail of all these open source projects and their use would be a vast readme.md file that would quickly become out of date.  For example, this document will state to "Install Node".  For the individual steps to install node, please go the Node main page.


## WebStarter application
This project contains a modern (Angular) web application that demonstrates all the automation testing features.  Sample configurations exists for all major browsers on desktops, emulators and physical devices and can be used to start a project for its specific requirements.  Use this application to:
  0. Learn about the ICC Automation frameworks with a live application
  1. Train and mentor new automation resources
  2. Practice automation techniques without needing a real application
  3. Copy and paste code into another project to automate another web application for additional mentoring or to start a real project


##  Pre-requisites for automation developer (SDET) machine
  0. Install Node & NPM (Latest)
  1. Global NPM Modules (Version will change over time, consult an Automation Lead)
    a. @angular/CLI
    b. protractor
    c. webdriver-manager 
    d. appium
    e. appium-doctor
    f. http-server
  3. Install java (1.8) 
  4. Install carthage (* Do not install via npm)
  5. Install android studio
  6. Install Xcode (Mac only)
  7. Major browsers: chrome, firefox & safari (Mac only) 
  8. Install VSCode (or use your preferred code editor)


##  Prep for Browser Testing
  0. webdriver update (installs latest versions of major browsers) 
  1. webdriver update "browser.version" (for specific drivers needed)
  2. webdrier
  3. safari - Add Develop menu item and set "Allow for Remote Automation"


##  Prep for Browser Testing with emulators/devices
  0. Run appium-doctor: This will check all requirements and will either fix issues or highlight manual resolutions required.
  1. android studio
    a. Using SDK Tool: Add one sdk to get started, then additional sdk's per project requirements
    b. Using AVD Tool: Add one virtual device (emulator)


##  Packages/frameworks used within the WebStarter project
  The following packages are in addition to the standard @angular/cli installs:
  0. cucumber
  1. protractor-cucumber-framework
  2. protractor-axe-html-report-plugin
  3. chai
  4. chai-as-promised


##  Install and verify the WebStarter application
  0. Download the WebStarter source code via git clone
  1. cd WebStarter
  2. npm install
  3. To run the application:  ng serve --port 4201
  4. To run unit tests:  ng test --code-coverage
  5. To view code coverage report:  http-server -c-1 -o -p 9875 ./coverage
  6. To run e2e chrome tests:  protractor ./e2e/config/chrome.conf.js
  7. To run accessibility tests:  ng e2e --protractor.config ./e2e/config/accessiblity.conf.js
  
  If all the above run successfully, the WebStarter project is a functional application and the automation framework is fully operational.

  If any steps above fail, please contact an SDET Lead for troubleshooting.


##  SDET Ramp Up
This readme.md is focused on ICC automation and use of the WebStarter project,and is not a training or reference manual for the dozens of technologies mentioned in previous sections.  Each SDET must have a working knowledge of every technology listed prior.  If an SDET does not have a working knowledge of any of these technologies, they must search for the publicly available sites, tutorials or videos, or contact an SDET Lead for suggestions. These resources will frequently change as the technologies change so listing resources here will quickly become obsolete.  A fresh search on these technologies is recommended.

In addition, each SDET must have a strong working knowledge of these technologies:
  0. javascript
  1. typescript
  2. html
  3. css
  4. json
  5. REST Api's
  6. cucumber
  7. gherkin 
  8. selenium
  9. appium
  10. android studio and related testing tools
  11. XCode and related testing tools
  12. postman
  13. node express
  13. jasmine unit testing
  14. TDD


##  Content of the WebStarter Project folders
The WebStarter project has the following folders in the root folder:
  0. e2e - Contains all the e2e automation logic
  1. node_modules - All the NPM packages
  2. src - All the source code for the demo web application

These files also reside in the root folder:
  0. angular.json - used by the angular cli tool, do not update
  1. package-lock.json - used by NPM, do not update
  2. package.json - use by npm for the WebStarter application and automation, do not update
  3. packageStarter.json - used when starting a new automation, instruction below
  4. tsconfig.json - used for the Typescript transpile and WebPack, do not update
  5. tslint.json - used to promote Typescipt linting (best practices) development

Within the e2e folder the following folders and files exist:
  0. accessiblity - contains the accessibilty Protractor/Jasmine specifications
  1. config - contains all the configuration file to run Protactor based tests
  2. features - contains all the cucumber feature files and subdivided in feature files
  3. global - constains the reusable functions
  4. json - contains data used for tests, subdivided by environment for each test user
  5. steps - constains the steps definition and mirrors the features subfolders and files


## Using WebStarter to start a new test automation effort on a ICC web application
The following are the basic highlevel tasks to start a new automation project:

  0. Start with a fully operational Death Star... err, I mean WebStarter project
  1. Delete the following:
    * node_modules
    * src
    * package.json
    * tsconfig.json
    * tslint.json 
  2. Rename the packageStarter.json to package.json
  3. Move the remaining content to the root folder of the web application's source repository
    * If the application already has a package.json, this content needs to be merged
    * If the application does not a source repository, then a new automation project is created
    * If the web application is on a different version of angular, some minor tweaks may be needed
  4. At the root directory run npm install
  5. Load the feature files
  6. Start functional automation iterative development
    a. Create page objects
    b. Create step definitions
    c. Update a configuration file
    d. Test to success
    e. Rinse and repeat
  7. Start accessibility automation development
    a. Create the accessibility specification
    b. Test for each page


## End of Document
