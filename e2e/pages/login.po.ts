import { browser, by, element } from 'protractor';

export class LoginPage {
  navigateTo() {
    return browser.get('/');
  }

  getCurrentUrl() {
    return browser.getCurrentUrl();
  }

  getLoginTitle() {
    return element(by.id('loginTitle'));
  }

  getLoginUserLabel() {
    return element(by.id('loginUserLabel'));
  }

  getLoginPasswordLabel() {
    return element(by.id('loginPasswordLabel'));
  }

  getLoginUserName() {
    return element(by.id('loginUserName'));
  }

  getLoginPassword() {
    return element(by.id('loginPassword'));
  }

  getLoginButton() {
    return element(by.id('loginSubmitButton'));
  }

  getLoginErrorMessage() {
    return element(by.id('loginErrorMessage'));
  }

}
