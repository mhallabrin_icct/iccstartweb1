import { browser } from 'protractor';
import { runAxeTest } from 'protractor-axe-html-report-plugin';
import { HomePage } from './accessiblity.po';

describe('ICC Automation page', () => {
  let page: HomePage;

  beforeEach(() => {
    page = new HomePage();
    page.navigateToLogin();
    page.getLoginUserName().sendKeys('Mike');
    page.getLoginPassword().sendKeys('Mike');
    page.getLoginSubmitButton().click();
    expect(page.getHomeTeam()).toEqual('ICC Automation Team');
  });

  it('should pass accessibility tests', function() {
    runAxeTest('Home page');
  });
});
