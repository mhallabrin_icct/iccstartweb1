import {setDefaultTimeout} from 'cucumber';
import { browser, by, element, ExpectedConditions, ElementFinder } from 'protractor';

setDefaultTimeout(15000);
console.log('****************************************************');
console.log(' ');
console.log('ICC Automation Tests Summary: ');
console.log('Application: ' + browser.params.app);
console.log('BaseUrl: ' + browser.baseUrl);
console.log('Browser: ' + browser.params.browser);
console.log('Viewport: ' + browser.params.viewport);
console.log('Runtime: ' + browser.params.runtime);
console.log('Pipeline: ' + browser.params.pipeline);
console.log('Test Suite ' + browser.params.test);
console.log('Date/Time: ' + Date());
console.log('Timeout set for 15 seconds');
console.log(' ');
console.log('****************************************************');
