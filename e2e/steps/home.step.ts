import { HomePage } from '../pages/home.po';
import { LoginPage } from '../pages/login.po';
import { Given, When, Then } from 'cucumber';
import { browser, by, element, ExpectedConditions } from 'protractor';
import * as chai from 'chai';
import * as cap from 'chai-as-promised';
import { ParseSourceSpan } from '@angular/compiler';
const jsonfile = require('jsonfile');

chai.use(cap);
const expect =  chai.expect;
const EC = ExpectedConditions;

const home: HomePage = new HomePage();
const login: LoginPage = new LoginPage();

let who;

Given(/^I am on the home page as ([^"]*)$/, logon);
  async function logon(user): Promise<void> {
    who = jsonfile.readFileSync('./e2e/json/local/' + user + '.json');
    login.navigateTo();
    await expect(login.getLoginTitle().getText()).to.eventually.equal('Login', 'Login Title does not match');
    await expect(login.getLoginUserLabel().getText()).to.eventually.equal('Username');
    await expect(login.getLoginPasswordLabel().getText()).to.eventually.equal('Password');
    await login.getLoginUserName().sendKeys(who.userName);
    await login.getLoginPassword().sendKeys(who.password);
// mjh: needed for android mobile animation
//    await browser.driver.sleep(1000);
    await login.getLoginButton().click();
  }

Then(/^the information on the home page is valid$/, homePage);
  async function homePage(): Promise<void> {
    await browser.wait(EC.textToBePresentInElement(home.getHomeTeam(), 'ICC Automation Team'), 5000);
    await expect(home.getHomeTeam().getText()).to.eventually.equal('ICC Automation Team');
    await expect(home.getUserRole().getText()).to.eventually.equal(who.role);
    await expect(home.getUserName().getText()).to.eventually.equal(who.name);
  }

  When(/^I add a team message$/, addMessage);
  async function addMessage(): Promise<void> {
    await expect(home.getMessages().count()).to.eventually.equal(3);
    await home.getNewMessage().sendKeys('My Team is Awesome');
    await home.getNewMessageBtn().click();
  }

  Then(/^the team message appears$/, checkMessage);
  async function checkMessage(): Promise<void> {
    await expect(home.getMessages().count()).to.eventually.equal(4);
    await browser.wait(EC.textToBePresentInElement(home.getMessage(0), 'ICC Meetup scheduled for next Friday'), 5000);
  }

  When(/^I complete the first item in the todo lists$/, completeTasks);
  async function completeTasks(): Promise<void> {
    await expect(home.getIccsToDo().count()).to.eventually.equal(who.ctd, 'Pre-Click: ICC ToDo Count Off');
    await expect(home.getIccsDone().count()).to.eventually.equal(who.cd, 'Pre-Click: ICC Done Count Off');
    await expect(home.getTeamsToDo().count()).to.eventually.equal(who.wtd, 'Pre-Click: Team ToDo Count Off');
    await expect(home.getTeamsDone().count()).to.eventually.equal(who.wd, 'Pre-Click: Team Done Count Off');
    await expect(home.getPersonalsToDo().count()).to.eventually.equal(who.btd, 'Pre-Click: Personal ToDo Count Off');
    await expect(home.getPersonalsDone().count()).to.eventually.equal(who.bd, 'Pre-Click: Personal Done Count Off');
    await home.getIccToDo(0).click();
    await home.getTeamToDo(0).click();
    await home.getPersonalToDo(0).click();
    await browser.wait(ExpectedConditions.presenceOf(home.getIccDone(who.cd)), 5000, 'Moved ICC Done task to not found.');
  }

  Then(/^the first item is in the done list$/, checkMovedTasks);
  async function checkMovedTasks(): Promise<void> {
    await expect(home.getIccsToDo().count()).to.eventually.equal(who.ctd - 1, 'Post-Click: ICC ToDo Count Off');
    await expect(home.getIccsDone().count()).to.eventually.equal(who.cd + 1, 'Post-Click: ICC Done Count Off');
    await expect(home.getTeamsToDo().count()).to.eventually.equal(who.wtd - 1, 'Post-Click: Team ToDo Count Off');
    await expect(home.getTeamsDone().count()).to.eventually.equal(who.wd + 1, 'Post-Click: Team Done Count Off');
    await expect(home.getPersonalsToDo().count()).to.eventually.equal(who.btd - 1, 'Post-Click: Personal ToDo Count Off');
    await expect(home.getPersonalsDone().count()).to.eventually.equal(who.bd + 1, 'Post-Click: Personal Done Count Off');
  }

  When(/^I un-complete the first item in the done lists$/, unCompleteTask);
  async function unCompleteTask(): Promise<void> {
    await home.getIccDone(0).click();
    await home.getTeamDone(0).click();
    await home.getPersonalDone(0).click();
  }

  Then(/^the first item is in the todo list$/, checkTasks);

  async function checkTasks(): Promise<void> {
    await expect(home.getIccsToDo().count()).to.eventually.equal(who.ctd, 'Final-Click: ICC ToDo Count Off');
    await expect(home.getIccsDone().count()).to.eventually.equal(who.cd, 'Final-Click: ICC Done Count Off');
    await expect(home.getTeamsToDo().count()).to.eventually.equal(who.wtd, 'Final-Click: Team ToDo Count Off');
    await expect(home.getTeamsDone().count()).to.eventually.equal(who.wd, 'Final-Click: Team Done Count Off');
    await expect(home.getPersonalsToDo().count()).to.eventually.equal(who.btd, 'Final-Click: Personal ToDo Count Off');
    await expect(home.getPersonalsDone().count()).to.eventually.equal(who.bd, 'Final-Click: Personal Done Count Off');

  }
