
const { SpecReporter } = require('jasmine-spec-reporter');

//  To update protractors webdriver-manager drivers run:  npm run webdriver

// seleniumAddress: 'http://localhost:4444/wd/hub',

exports.config = {
  directConnect: true,

  capabilities: {
  'browserName': 'chrome'
 },

 params: {viewport: 'lg',
    browser: 'chrome',
    app: 'Automation Team Website',
    test: 'Full Regression Suite',
    runtime: 'Local',
    pipeline: 'TBD'
 },

 baseUrl: 'http://localhost:4201/',
//    baseUrl: 'http://localhost:80/',     // for docker
//   baseUrl: 'http://10.0.2.2/4201',      // for android
//    baseUrl: 'http://localhost./',

  // mjh : To run Cucumber run below:
  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],

  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../**/*.step.ts'],       // require step definition files before executing features
    tags: [],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },
  // end new cucumber things

  // old Jasmine things: can stay
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },

  allScriptsTimeout: 11000,

  onPrepare() {
    var width;
    var height;
    switch (browser.params.viewport) {
      case 'xs':
        width = 450;
        height = 600;
        break;
      case 'sm':
        width = 550;
        height = 600;
        break;
      case 'md':
        width = 800;
        height = 600;
        break;
      case 'lg':
        width = 900;
        height = 700;
        break;
      case 'xl':
        width = 1100;
        height = 750;
        break;
      default:
        width = 800;
        height = 600;
        break;
    }
    browser.driver
      .manage()
      .window()
      .setSize(width, height);

    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
//mjh; needed for jasmine, remove for cucumber
//    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};
