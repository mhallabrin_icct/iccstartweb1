const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {

  directConnect: true,

  allScriptsTimeout: 11000,
  capabilities:
  {
    'browserName': 'firefox',
    firefoxOptions: {
      args: ['--headless']
    },
    'moz:firefoxOptions': {
      args: [ '--headless' ]
    }
  },

  baseUrl: 'http://localhost:4201/',

   params: {viewport: 'na',
    browser: 'firefox',
    app: 'StarterProjectWeb',
    test: 'Full Regression Suite',
    runtime: 'local',
    pipeline: 'AdHoc'
   },

  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],

  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../steps/*.step.ts'],  // require step definition files before executing features
    tags: [],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },
  // end new cucumber things

  // old Jasmine things: can stay
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  }
};
