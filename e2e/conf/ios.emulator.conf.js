const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  directConnect: false,
  seleniumAddress: 'http://localhost:4723/wd/hub', //appium

  capabilities: {
    browserName: 'safari',
    platformName: 'iOS',
 // iOS Simulator
    automationName: 'XCUITest',
    platfomrVersion: '11.2',
    deviceName: 'iPhone Simulator'


//  platformVersion: '11.3.1',
//  deviceName: 'Donis iPhone',

//  Physical Device
//    automationName: 'XCUITest',
//    platformVersion: '9.3',
//    deviceName: 'CCQJQUSQF4K1',
//    deviceName: 'Michael Touch',

},

params: {viewport: 'xs',
browser: 'safari',
app: 'ICC Automation',
test: 'Full Regression Suite',
runtime: 'iPhone Touch',
pipeline: 'On Commit Pipeline'
},

  baseUrl: 'http://localhost:4201/',
//  baseUrl: 'http://localhost./',

  // mjh : To run Cucumber run below:
  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],

  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../**/*.step.ts'],  // require step definition files before executing features
    tags: [],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },

  allScriptsTimeout: 11000,

  onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  }
};
