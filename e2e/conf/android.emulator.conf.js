// windows
// appium --chromedriver-executable c:\Users\michael.j.hallabrin\AppData\Roaming\npm\node_modules\protractor\node_modules\webdriver-manager\selenium\chromedriver_2.34
// mac
// appium --chromedriver-executable /Users/mhallabrin/node/lib/node_modules/protractor/node_modules/webdriver-manager/selenium/chromedriver_2.34
//
//  webdriver-manager update --versions.chrome 2.34

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {

  seleniumAddress: 'http://localhost:4723/wd/hub', //appium

  directConnect: false,

  capabilities: {

  //  Android TBD
    browserName: 'chrome',
    platformName: 'Android',
    deviceName: 'Galaxy Nexus API 27'   // emulator
//    deviceName: 'android 24 google apis x86 64 v24.4.1 wd manager'   // emulator
//    deviceName: 'Mikes'               // physical device

},

params: {viewport: 'na',
  browser: 'chrome',
  app: 'StarterProjectweb',
  test: 'Full Regression Suite',
  runtime: 'Local',
  pipeline: 'AdHoc'
},

  baseUrl: 'http://10.0.2.2:4201',     // for emulator
//  baseUrl: 'http://c64ad538.ngrok.io',
//  baseUrl: 'http://localhost./',

  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],

  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../**/*.step.ts'],  // require step definition files before executing features
    tags: ['@regression'],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },

 allScriptsTimeout: 11000,

  onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  }
};
