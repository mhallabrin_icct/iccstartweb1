const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {

  directConnect: true,

  seleniumAddress: 'http://localhost:4444/wd/hub',

  allScriptsTimeout: 11000,
  capabilities: {
    'browserName': 'chrome'
  },

   params: {viewport: 'lg',
    browser: 'chrome',
    app: 'StarterProjectWeb',
    test: 'Full Regression Suite',
    runtime: 'local',
    pipeline: 'AdHoc'
   },

  baseUrl: 'http://localhost:4201/',

  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],

  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../**/*.step.ts'],  // require step definition files before executing features
    tags: [],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },

  onPrepare() {
    console.log('mjh: in onPrepare viewport = ' + browser.params.viewport);
    var width;
    var height;
    switch (browser.params.viewport) {
      case 'xs':
        width = 400;
        height = 600;
        break;
      case 'sm':
        width = 500;
        height = 600;
        break;
      case 'md':
        width = 800;
        height = 600;
        break;
      case 'lg':
        width = 1000;
        height = 800;
        break;
      case 'xl':
        width = 1100;
        height = 900;
        break;
      default:
        width = 1000;
        height = 800;
        break;
    }
    browser.driver
      .manage()
      .window()
      .setSize(width, height);

    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  }
};
